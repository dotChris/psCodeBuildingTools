Import-Module ../src/CppBuild.psm1 -Force 3> $null;

Describe 'GCC based C/C++ Building Tools' {
    BeforeAll {
        Copy-Item -Recurse -Path ./data -Destination TestDrive:/Libs;
    }
    Context 'object file creation' {
        
        It 'creates object file from one c file' {
            
            $cFile = Join-Path $TestDrive 'Libs/Util/math.c';
            $objFile = Build-CppObjFiles -CppPaths $cFile -Default;

            # check output 
            $objFile.Name | Should Be "math.o"
            # check directory creation 
            $objFile.Directory.Name | Should Be "obj"
            # check correct location of math.o 
            (Get-ChildItem TestDrive:/Libs/Util/obj/*.o).Name | Should Be "math.o"
        }
        It 'overwrites existing object file from one c file' {
            
            $cFile = Join-Path $TestDrive 'Libs/Util/math.c';
            $objFile = Build-CppObjFiles -CppPaths $cFile -Default;

            # check output 
            $objFile.Name | Should Be "math.o"
            # check directory creation 
            $objFile.Directory.Name | Should Be "obj"
            # check correct location of math.o 
            (Get-ChildItem TestDrive:/Libs/Util/obj/*.o).Name | Should Be "math.o"

        }
        It 'creates two object files from two c files' {

            $cFiles = @( (Join-Path $TestDrive 'Libs/Util/math.c'), `
                         (Join-Path $TestDrive 'Libs/Util/strings.c'));
            
            $objFiles = Build-CppObjFiles -CppPaths $cFiles -Default;
            
            # check number 
            $objFiles.Count | Should Be 2
            # check first argument 
            $objFiles[0].Name | Should Be "math.o"
            # check second argument 
            $objFiles[1].Name | Should Be "strings.o"
            # check folder structure 
            $foundObjFiles = (Get-ChildItem TestDrive:/Libs/Util/obj/*.o);
            $foundObjFiles.Count | should be 2
            $foundObjFiles[0].Name | Should be "math.o"
            $foundObjFiles[1].Name | Should be "strings.o"

        }
    }
    Context 'Link objects' {
        BeforeAll {
            $cFiles = @(    (Join-Path $TestDrive 'Libs/Util/math.c'), `
                            (Join-Path $TestDrive 'Libs/Util/strings.c'));

            Build-CppObjFiles -CppPaths $cFiles -Default > $null;
        }
        It 'creates one dynamic lib by just one object file' {
            $objectFile = Join-Path $TestDrive 'Libs/Util/obj/math.o';
            $dynLib = Link-CppObjFiles -ObjectFiles $objectFile;
            
            # check if is not empty 
            $dynLib | Should Not Be $null
            # check if correct type 
            $dynLib.GetType().Name | Should Be "FileInfo"
            # check name is ok 
            $dynLib.BaseName | Should Be "Util"
            
        }
    }
    Context 'Create dyn Lib from C file' {
        BeforeAll {
            Copy-Item -Recurse -Path ./data -Destination TestDrive:/Libs;
        }
        It 'creates one dll from one C file which depend on other c files' {
            
            $cFile = Join-Path $TestDrive 'Libs/Filter/Filter.c';
            $output = Join-Path $TestDrive 'Libs/Filter/bin/Filter.dll';

            $dynLib = Build-CppDynLib -CppPaths $cFile -OutputName $output;

            # check if is not empty 
            $dynLib | Should Not Be $null
            # check if correct type 
            $dynLib.GetType().Name | Should Be "FileInfo"
            # check name is ok 
            $dynLib.BaseName | Should Be "Filter"
                        
        }
    }
}
if ($env:OS -eq 'Windows_NT'){
    $dynLibExt = 'dll';
}
else {
    $dynLibExt = 'so';
}
function Build-CppObjFiles {
    param(
        # c or cpp files
        [parameter(Position = 0)]
        [string[]]
        $CppPaths,
        # default destinaztion
        [parameter(Position = 1)]
        [switch]
        $DefaultOutput = $false
    )
    begin {
        $cppObjDirectory = @(); 
        if ($DefaultOutput) {
            for ($idx = 0; $idx -lt $CppPaths.Count; $idx++) {
                $cppObjDirectory += ((Split-Path -Path $CppPaths[$idx] -Parent) + `
                                    [IO.Path]::DirectorySeparatorChar + 'obj' + [IO.Path]::DirectorySeparatorChar);
            }

        }
    }
    process {
        
        $objectFiles = @();

        for ($idx = 0; $idx -lt $CppPaths.Count; $idx++) {
            $idxCppFileObject = Get-Item $CppPaths[$idx];
            $outputFile =  $cppObjDirectory[$idx] + $idxCppFileObject.BaseName.ToString() + '.o';

            if ((Test-Path $cppObjDirectory[$idx]) -eq $false) {
                [void](New-Item -ItemType Directory -Path $cppObjDirectory[$idx]);
            }
            else  {
                # pass 
            }

            $inputFile =  (Get-Item $CppPaths[$idx] ).FullName;
            $buildCmd = 'gcc -c -o "' + $outputFile + '" "' + $inputFile +'"'  ;    
            Invoke-Expression $buildCmd;
            $objectFiles += [System.IO.FileSystemInfo] (Get-Item $outputFile);
        }
        
    }
    end {
       
        return $objectFiles;
    }
}
function Link-CppObjFiles {
    param(
        # list of object files
        [Parameter(Position =0)]
        [string[]]
        $ObjectFiles,
        # output location
        [Parameter(Position = 1)]
        [string]
        $OutputName,
        # executable output 
        [Parameter(Position = 2)]
        [switch]
        $Exe
    ) 
    begin {
        [System.Object[]]$objectChecker = $ObjectFiles   | ForEach-Object {((Get-Item $_).Extension -eq '.o') -and (Test-Path $_) };
        if ($objectChecker.Contains($false) -eq $true) {
            # error!!!! 
        }

        [System.IO.FileSystemInfo[]]$objectFilesInfo = $ObjectFiles | ForEach-Object { Get-Item $_ }; 
        
        if ($OutputName -eq "") {
            [string[]]$objectFilesDir = $objectFilesInfo    | ForEach-Object { $_.Directory.FullName} `
                                | Select-Object -Unique;                                
            if ($objectFilesInfo.Count -eq 1) {
                $projectItem = Get-Item (Split-Path -Path $objectFilesDir[0] -Parent);
                $OutputName = Join-Path $projectItem.FullName ('bin/' + $projectItem.BaseName + '.' + $dynLibExt  );
            }                                
        }
        else {
            # pass 
        }

        $binDir = Split-Path -Path $OutputName -Parent;

        if ((Test-Path $binDir) -eq $false) {
            [void](New-Item -ItemType Directory -Path $binDir);
        }
        else {
            #pass
        }

    }

    process {

        $linkCmd = 'gcc -shared -o "' + $OutputName + '" ';

        foreach ($objFile in $objectFilesInfo) {
            $linkCmd = $linkCmd + '"' +$objFile.FullName + '" '; 
        }

        Invoke-Expression $linkCmd;
    }
    
    end {
        $dynLibObj = Get-Item $OutputName;
        return $dynLibObj;
    }
}
function Build-CppDynLib {
    param(
        # list of cpp / c files
        [Parameter(Position = 0)]
        [string[]]
        $CppPaths, 
        # output name 
        [Parameter(Position = 1)]
        [string]
        $OutputName, 
        # exe or not 
        [Parameter(Position = 2)]
        [switch]
        $Exe = $false
    )
    begin {
        [string[]]$cppDepFiles = $CppPaths  | ForEach-Object { Get-IncFolders $_} `
                                            | Select-Object -Unique;
        $CppPaths += $cppDepFiles;

        $objFiles = Build-CppObjFiles -CppPaths $CppPaths -DefaultOutput;
        
        $dynLib = Link-CppObjFiles -ObjectFiles $objFiles -OutputName $OutputName;
    }
    end {
        $dynLibInfo = Get-Item $dynLib;
        return $dynLibInfo;
    }
}

function New-ObjectFile {
    
    Param(
		[parameter(Position=0)]
		[string]$Path,
		[parameter(Position=1)]
        [string]$Destination = `
                (Split-Path -Path $Path -Parent)  + `
                [IO.Path]::DirectorySeparatorChar + 'obj' + [IO.Path]::DirectorySeparatorChar + `
                (Get-Item -Path $Path).BaseName.ToString() + '.o'    
        )
        
    begin {
        
        if ((Test-Path $Destination) -eq $false) {
            $objFolder = Split-Path -Path $Destination -Parent;
            if ((Test-Path $objFolder ) -eq $false) { New-Item -ItemType Directory $objFolder};
            [void](New-Item -ItemType File $Destination);
        }

        [void](Remove-Item -Path $Destination);
    }
    
    process {
        
        $command = 'gcc -c -o "' + $Destination + '"  "' + $Path +'"' ;
        [void](Invoke-Expression $command);
    }
    
    end {
        $objFile = Get-Item -Path $Destination;
        return;
    }
}
function New-ObjectFiles {
   
    param(
    [parameter(Position=0)]
	[string]$Path,
	[parameter(Position=1)]
    [string]$Destination = `
        $Path  + [IO.Path]::DirectorySeparatorChar + 'obj' 
    )
    
    begin {
        [System.IO.FileSystemInfo[]] $cppFiles = Get-ChildItem -Path ($Path + [System.IO.Path]::DirectorySeparatorChar + '*.cpp');
        [System.IO.FileSystemInfo[]] $cFiles = Get-ChildItem -Path ($Path + [System.IO.Path]::DirectorySeparatorChar + '*.c');

        [System.IO.FileSystemInfo[]] $srcFiles = $cppFiles + $cFiles;
    }
    
    process {
        $dstFileList = New-Object 'System.Collections.Generic.List[System.IO.FileSystemInfo]';

        for ($idx = 0; $idx -lt $srcFiles.Count; $idx++) {
            $puffer = New-ObjectFile -Path $srcFiles[$idx];
            $dstFileList.Add($puffer);
        }
    }
    
    end {
        
        [System.IO.FileSystemInfo[]] $objFiles = [System.Linq.Enumerable]::ToArray($dstFileList);
        return $objFiles;
    }
}
function New-DynamicLib {
    
    param (
        [Parameter(Position=0)]
        [string[]]
        $Path,
        [Parameter(Position=1)]
        [string[]]
        $Destination, 
        [Parameter(Position = 2)]
        [switch]$MultipleLibs 
    )
    
    begin {

        if ($MultipleLibs){
                 
        }
        else {

            if ($Destination -eq $null) {
                $Destination =  ((Get-Item -Path $Path[0]).Directory.ToString()  + `
                            [IO.Path]::DirectorySeparatorChar + 'bin' + [IO.Path]::DirectorySeparatorChar + `
                            (Get-Item -Path $Path[0]).Directory.BaseName.ToString() + '.' + $dynLibExt);
            }
            else {
                # pass 
            }

            if ((Test-Path $Destination[0]) -eq $false)
            {
                $parentDestination = Split-Path -Path $Destination[0] -Parent;
                New-Item -Path $parentDestination -ItemType Directory;
                New-Item -Path $Destination[0] -ItemType File;
            }
    
            Remove-Item -Path $Destination[0];    
        }
        
    }
    
    process {
            [string[]]$incFolders =  $Path      | ForEach-Object {Get-IncFolders -Path $_} `
                                                | Select-Object -Unique  ;

            $objFileList = New-Object 'System.Collections.Generic.List[System.IO.FileSystemInfo]';;

            $incFolders | ForEach-Object {  
                                            $puffer = New-ObjectFiles -Path ((GEt-Item $_).FullName);
                                            $objFileList = $objFileList + $puffer
                                         };

            $gccCommand = 'gcc -shared -o "' + $Destination + '" ';
            
            $objFileList | ForEach-Object { $gccCommand = $gccCommand + '"' + $_.FullName + '" ' };

            #$gccCommand = $gccCommand +  '"' + $cFileObjFile + '"';
        
            Invoke-Expression $gccCommand;
        
        }
    
    end {
        [System.IO.FileSystemInfo[]]$dynLibs = $Destination | ForEach-Object {Get-Item $_};
        return $dynLibs;
    }
}
function Get-IncFolders {
    param (
        [Parameter(Position = 0)]
        [string]
        $Path       
    )
    
    begin {
    }
    
    process {
        [string[]] $srcFileContent = Get-Content -Path $Path;
        [String[]] $includedHeaders = $srcFileContent | Where-Object { $_.Contains('#include') } `
        | ForEach-Object {$_ -replace '#include ',''} `
        | ForEach-Object {$_ -replace '"',''} `
        | Where-Object {$_.Contains('..')};

        [String[]] $resolvedPath = $includedHeaders    | ForEach-Object {Resolve-Path ( (Get-Item $Path).Directory.FullName + '/' + $_ )} ;
        
        [string[]]$resolvedDirs = $resolvedPath   | ForEach-Object {(Get-Item ($_)).FullName} `
                        | Select-Object -Unique `
                        | ForEach-Object {$_.Replace('.h','.c')}               
    }
    
    end {
        return $resolvedDirs ;
    }
}

